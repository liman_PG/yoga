class CreateTrainings < ActiveRecord::Migration[5.0]
  def change
    create_table :trainings do |t|
      t.date :date_start
      t.date :date_end
      t.string :place
      t.string :title
      t.text :body
      t.string :url
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
