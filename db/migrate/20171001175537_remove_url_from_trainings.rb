class RemoveUrlFromTrainings < ActiveRecord::Migration[5.0]
  def change
    remove_column :trainings, :url, :string
  end
end
