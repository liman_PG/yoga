class RemoveDescriptionAndKeywordsFromPages < ActiveRecord::Migration[5.0]
  def change
    remove_column :pages, :description, :string
    remove_column :pages, :keywords, :string
  end
end
