class AddAvatarToTrainings < ActiveRecord::Migration[5.0]
  def change
    add_column :trainings, :avatar, :string
  end
end
