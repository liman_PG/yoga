class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :url
      t.string :title
      t.string :description
      t.string :keywords
      t.text :body

      t.timestamps
    end
  end
end
