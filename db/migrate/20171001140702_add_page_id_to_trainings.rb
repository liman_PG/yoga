class AddPageIdToTrainings < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :trainings, :page, index: true
  end
end
