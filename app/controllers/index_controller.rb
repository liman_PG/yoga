class IndexController < ApplicationController
  def index
    @index_page = Page.find_by_url('index')
  end
end
