class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :set_layout

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def set_layout
    devise_controller? ? 'one-solid-box' : 'application'
  end
end
