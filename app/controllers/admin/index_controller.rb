class Admin::IndexController < Admin::AdminController
  def index
    redirect_to admin_pages_path
  end
end
