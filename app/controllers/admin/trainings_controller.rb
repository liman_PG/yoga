class Admin::TrainingsController < Admin::AdminController
  before_action :set_training, only: [:show, :edit, :update, :destroy]
  before_action :clear_temporary_page, :set_temporary_page, only: :create
  after_action :clear_temporary_page, only: :create

  # GET /trainings
  def index
    @trainings = Training.all
  end

  # GET /trainings/1
  # GET /trainings/1.json
  def show
  end

  # GET /trainings/new
  def new
    @training = Training.new
  end

  # POST /trainings
  # POST /trainings.json
  def create
    @training = Training.new(training_params.merge(page_id: @page.id))
    respond_to do |format|
      if @training.save
        format.html { redirect_to admin_training_path(@training), notice: 'Training was successfully created.' }
        format.json { render :show, status: :created, location: @training }
      else
        format.html { render :new }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trainings/1
  # PATCH/PUT /trainings/1.json
  def update
    respond_to do |format|
      if @training.update(training_params)
        format.html { redirect_to admin_training_path(@training), notice: 'Training was successfully updated.' }
        format.json { render :show, status: :ok, location: @training }
        @training.update_temporary_page
      else
        format.html { render :edit }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trainings/1
  # DELETE /trainings/1.json
  def destroy
    @training.destroy
    respond_to do |format|
      format.html { redirect_to admin_trainings_url, notice: 'Training was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_training
      @training = Training.find(params[:id])
    end

    # Create temporary object Page for create Training with page_id
    def set_temporary_page
      @page = Page.create(url: "temporary", title: "temporary", body: "temporary")
    end

    def clear_temporary_page
      page = Page.find_by(url: "temporary", title: "temporary", body: "temporary")
      page.delete if page
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def training_params
      params.require(:training).permit(:date_start, :date_end, :place, :active, :title, :body, :avatar)
    end
end
