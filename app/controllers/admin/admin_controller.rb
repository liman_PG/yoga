class Admin::AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :check_permissions

  layout 'one-solid-box'

  private

  def check_permissions
    unless current_user.has_role? :admin
      flash[:alert] = t(:you_dont_have_permission)
      redirect_back(fallback_location: root_path)
    end
  end


end
