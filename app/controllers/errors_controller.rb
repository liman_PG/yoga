class ErrorsController < ApplicationController

  layout 'one-solid-box'

  def error404
    render status: :not_found
  end
end
