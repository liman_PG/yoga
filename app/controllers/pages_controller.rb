class PagesController < ApplicationController
  layout 'one-solid-box'

  def show
    @page = Page.find_by_url_icase(params[:url]) || not_found

  end
end
