$ ->
  $(window).scroll ->
    if $(this).scrollTop() < 200
      $('#totop').fadeOut()
    else
      $('#totop').fadeIn()
    return
  $('#totop').on 'click', ->
    $('html, body').animate { scrollTop: 0 }, 'fast'
    false
