json.array!(@pages) do |page|
  json.extract! page, :id, :url, :title, :description, :keywords, :body
  json.url page_url(page, format: :json)
end
