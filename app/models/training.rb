class Training < ApplicationRecord
  belongs_to :page, dependent: :destroy

  after_create :update_temporary_page

  validates_presence_of :date_start, :date_end, :place, :title, :body, :avatar
  validates :active, inclusion: { in: [ true, false ] }

  mount_uploader :avatar, AvatarUploader

  # Update params for page
  def update_temporary_page
    self.page.update(url: "trainings/#{ self.id }", title: "#{ self.title }", body: "#{ self.body }")
  end
end
