class Page < ApplicationRecord
  has_one :training, dependent: :destroy

  default_scope{ order(:url) }

  validates_presence_of :url, :title, :body
  validates_uniqueness_of :url

  def self.find_by_url_icase(url)
    self.where('LOWER(url) = ?', url.mb_chars.downcase).first
  end

  def description
    title + ' ' + I18n.t(:index_description)
  end

  def keywords
    title + ', ' + I18n.t(:index_keywords)
  end
end
