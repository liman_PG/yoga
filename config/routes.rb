Rails.application.routes.draw do


  get 'pages/show'

  devise_for :users
  mount Ckeditor::Engine => '/ckeditor'
  root 'index#index'







  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'


  namespace :admin do
    root 'index#index'
    resources :pages do
      resources :trainings, shallow: true
    end
    get 'trainings', to: 'trainings#index', as: :trainings
    post 'trainings', to: 'trainings#create'
    get 'training/new', to: 'trainings#new', as: :new_training
  end

  match '/404' => 'errors#error404', via: [ :get, :post, :patch, :delete ]


  get '(*url)', to: 'pages#show', as: :page_show, url: /.*/
end
